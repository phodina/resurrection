#!/bin/bash

# Based on https://theprivacyguide1.github.io/linux_hardening_guide.html

## Kernel
# Hide kernel symbols in /proc/kallsyms
echo "kernel.kptr_restrict=2" >> /etc/sysctl.d/kptr_restrict.conf

# Block everyones but root from accessing kernel logs
echo "kernel.dmesg_restrict=1" >> /etc/sysctl.d/dmesg_restrict.conf

# Only root can use the BPF JIT compiler and to harden it
echo "kernel.unprivileged_bpf_disabled=1" >> /etc/sysctl.d/harden_bpf.conf
echo "net.core.bpf_jit_harden=2" >> /etc/sysctl.d/harden_bpf.conf

# Only root is able to use ptrace
echo "net.core.bpf_jit_harden=2" >> /etc/sysctl.d/ptrace_scope.conf

# Disables kexec (used to replace the running kernel)
echo "kernel.kexec_load_disabled=1" >> /etc/sysctl.d/kexec.conf

# Helps protect against SYN flood attacks (DOS attack protection)
echo "net.ipv4.tcp_syncookies=1" >> /etc/sysctl.d/tcp_hardening.conf

# Protects against time-wait assassination (drops RST packets for sockets in the time-wait state)
echo "net.ipv4.tcp_rfc1337=1" >> /etc/sysctl.d/tcp_hardening.conf

# Enable source validation of packets received from all interfaces (protection against spoofing)
echo "net.ipv4.conf.default.rp_filter=1" >> /etc/sysctl.d/tcp_hardening.conf
echo "net.ipv4.conf.all.rp_filter=1" >> /etc/sysctl.d/tcp_hardening.conf

# Disable ICMP redirect acceptance
echo "net.ipv4.conf.all.accept_redirects=0" >> /etc/sysctl.d/tcp_hardening.conf
echo "net.ipv4.conf.default.accept_redirects=0" >> /etc/sysctl.d/tcp_hardening.conf
echo "net.ipv4.conf.all.secure_redirects=0" >> /etc/sysctl.d/tcp_hardening.conf
echo "net.ipv4.conf.default.secure_redirects=0" >> /etc/sysctl.d/tcp_hardening.conf
echo "net.ipv6.conf.all.accept_redirects=0" >> /etc/sysctl.d/tcp_hardening.conf
echo "net.ipv6.conf.default.accept_redirects=0" >> /etc/sysctl.d/tcp_hardening.conf

#Disable ICMP redirect sending
echo "net.ipv4.conf.all.send_redirects=0" >> /etc/sysctl.d/tcp_hardening.conf
echo "net.ipv4.conf.default.send_redirects=0" >> /etc/sysctl.d/tcp_hardening.conf

# Ignore all ICMP requests
echo "net.ipv4.icmp_echo_ignore_all=1" >> /etc/sysctl.d/tcp_hardening.conf

# Improve ASLR effectiveness for mmap
echo "vm.mmap_rnd_bits=32" >> /etc/sysctl.d/mmap_aslr.conf
echo "vm.mmap_rnd_compat_bits=16" >> /etc/sysctl.d/mmap_aslr.conf

# Disable TCP timestamps (could be used to calculate system uptime and boot time of machine)
echo "net.ipv4.tcp_timestamps=0" >> /etc/sysctl.d/tcp_timestamps.conf

# Disables SysRq key (which can make the kernel run certain commands)
echo "kernel.sysrq=0" >> /etc/sysctl.d/sysrq.conf

# Disables unprivileged user namespaces
echo "kernel.unprivileged_userns_clone=0 " >> /etc/sysctl.d/unprivileged_users_clone.conf

# Disables TCP SACK
echo "net.ipv4.tcp_sack=0 " >> /etc/sysctl.d/tcp_sack.conf


## Bootloader
# TODO: Add to kernel "slab_nomerge", "slub_debug=FZP", "page_poison=1", "pti=on", "module.sig_enforce=1", audit=1
# slab_nomerge disables the merging of slabs of similar sizes
# slub_debug=FZP enables sanity checks (F), redzoning (Z) and poisoning (P).
# page_poison=1 This makes the kernel overwrite freed memory so it can't leak
# "pti=on" This enables Kernel Page Table Isolation which mitigates Meltdown and prevents some KASLR bypasses.
# "module.sig_enforce=1" This only allows kernel modules signed with a valid key to be loaded
# audit=1 Use AppArmor

## Hide PIDs
# /etc/fstab >> proc /proc proc nosuid,nodev,noexec,hidepid=2,gid=proc 0 0 
# /etc/systemd/system/systemd-logind.service.d/hidepid.conf 
#	[Service]
#	SupplementaryGroups=proc 

## Netfilter
#/etc/modprobe.d/no-conntrack-helper.conf
#	options nf_conntrack nf_conntrack_helper=0 

## Linux headers
# TODO: Install hardened kernel 
pacman -Ss linux-hardened-headers linux-hardened

## AppArmor (MAC)
# TODO: Install apparmor, enable apparmor service and set kernel param
# To enable caching of the profiles to increase boot speed, uncomment 'write-cache' in /etc/apparmor/parser.conf.

## Firejail or Bubblewrap

## Root account
# TODO: check that /etc/seucretty is empty

# Restrict 'su' command to 'wheel' users
#/etc/pam.d/su and /etc/pam.d/su-l -- uncomment auth required pam_wheel.so use_uid 
chgrp wheel /usr/bin/su
chmod 4550 /usr/bin/su 
# Lock down root account (users must use sudo)
passwd --lock root

# Disallow root login over SSH
#/etc/ssh/sshd_config PermitRootLogin no 

# Increase the number of hashing rounds
# /etc/pam.d/passwd password required pam_unix.so sha512 shadow nullok rounds=65536 

# Passwords needs to be rehashed
# passwd username

## SystemD sandbox
## You can analyze e.g. systemd-analyze security tor.service 

## MAC Address spoofing
# (Install macchanger and add systemd script (macchanger -e spoofs only vendor bits))
# Network manager script to randomize the MAC address 
#/etc/NetworkManager/conf.d/rand_mac.conf 
#	[connection-mac-randomization]
#	ethernet.cloned-mac-address=random
#	wifi.cloned-mac-address=random 

## UMask
# Change /etc/profile from 022 to 0077 (makes new files not readable by anyone other than the owner)


## QEMU/KVM
# Install Virt-manager and GNOME Boxes

## Core dumps
# Disable core dumps for sysctl
echo "kernel.core_pattern=|/bin/false" >> /etc/sysctl.d/coredump.conf 
# Disable core dumps for systemd
mkdir -p /etc/systemd/coredump.conf.d/
echo "[Coredump]" >> /etc/systemd/coredump.conf.d/custom.conf
echo "Storage=none" >> /etc/systemd/coredump.conf.d/custom.conf
# Disable core dumps for ulimit
echo "* hard core 0" >> /etc/security/limits.conf

## NTP
# Disable NTP
systemctl disable systemd-timesyncd.service 

## IPv6
# TODO: Get the correct IF name
# Generate a random IPv6 address out of your original one to prevent you from being tracked
echo "net.ipv6.conf.all.use_tempaddr = 2" >> /etc/sysctl/ipv6_privacy.conf
echo "net.ipv6.conf.default.use_tempaddr = 2" >> /etc/sysctl/ipv6_privacy.conf
echo "net.ipv6.conf.eth0.use_tempaddr = 2" >> /etc/sysctl/ipv6_privacy.conf
echo "net.ipv6.conf.wlan0.use_tempaddr = 2" >> /etc/sysctl/ipv6_privacy.conf

# Enable privacy extensions for NetworkManager
echo "[connection]" >> /etc/NetworkManager/NetworkManager.conf
echo "ipv6.ip6-privacy=2" >> /etc/NetworkManager/NetworkManager.conf

# Enable privacy extensions for systemd-networkd
echo "[Network]" >> /etc/systemd/network/ipv6.conf
echo "IPv6PrivacyExtensions=kernel" >> /etc/systemd/network/ipv6.conf

## Blacklist network protocols
cat > /etc/modprobe.d/uncommon-network-protocols.conf <<EOL
install dccp /bin/true
install sctp /bin/true
install rds /bin/true
install tipc /bin/true
install n-hdlc /bin/true
install ax25 /bin/true
install netrom /bin/true
install x25 /bin/true
install rose /bin/true
install decnet /bin/true
install econet /bin/true
install af_802154 /bin/true
install ipx /bin/true
install appletalk /bin/true
install psnap /bin/true
install p8023 /bin/true
install llc /bin/true
install p8022 /bin/true 
EOL

## Partitioning and Mount Options
#nodev - Do not interpret character or block special devices on the file system. 
#nosuid - Do not allow setuid or setgid bits to take effect. 
#noexec - Do not allow direct execution of any binaries on the mounted file system

# /        /          ext4    defaults                      1 1
# /tmp     /tmp       ext4    defaults,nosuid,noexec,nodev  1 2
# /home    /home      ext4    defaults,nosuid,nodev         1 2
# /var     /var       ext4    defaults,nosuid               1 2
# /boot    /boot      ext4    defaults,nosuid,noexec,nodev  1 2
# /tmp     /var/tmp   ext4    defaults,bind,nosuid,noexec,nodev 1 2

## Blacklist mounting of Filesystems
cat > /etc/modprobe.d/uncommon-filesystems.conf<<EOL
install cramfs /bin/true
install freevxfs /bin/true
install jffs2 /bin/true
install hfs /bin/true
install hfsplus /bin/true
install squashfs /bin/true
install udf /bin/true 
EOL

## Entropy
# Random generator that gathers more entropy
pacman -S haveged
systemctl enable --now haveged.service 

## Hardened malloc
# Install hardened-malloc-git from AUR

## Login
# Delay after failed login (in micro seconds)
echo "auth optional pam_faildelay.so delay=4000000" >> /etc/pam.d/system-login
# Wait 600 seconds after 3 failed attempts
echo "auth required pam_tally2.so deny=3 unlock_time=600 onerr=succeed file=/var/log/tallylog" >> /etc/pam.d/system-login

## Limit number of processes and resources
# TODO

## DNSSec & VPN setup

## USBGuard
# TODO: Configure whitelist
pacman usbguard usbguard-qt
#!/bin/sh

set -e

mount_args(){
	if [ -z "$2" ];then
		echo "Usage: $0 mount <MOUNT_POINT>";
		exit 1
	fi

	MNT=$2
}

cryptsystem_exists(){
	if [ -e "/dev/disk/by-partlabel/cryptsystem" ];then
		echo "System doesn't contain 'cryptsystem' partition!"
		exit 1
	fi
}

mount_system(){
	# Install tools for Yubikey to work
	pacman -Sy --noconfirm yubikey-manager yubikey-personalization pcsc-tools libu2f-host make json-c cryptsetup git make
	systemctl start pcscd.service

	# Clone Yubikey full disk encryption
	git clone https://github.com/agherzan/yubikey-full-disk-encryption.git /tmp/yubikey-full-disk-encryption
	cd /tmp/yubikey-full-disk-encryption
	make install

	while true
	do
		[ -n  "$(ykman list)" ] && break
		echo "Please insert Yubikey ..."
		sleep 5
	done

	ykfde-open -d /dev/disk/by-partlabel/cryptsystem -n system
	mount /dev/vg/lv_root ${MNT}
	echo "System mounted to '/mnt'"
}

mount_args
cryptsystem_exists
mount_system

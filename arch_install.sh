#!/bin/sh

set -e
set -u

ARCH_INSTALL="arch.sh arch_chroot.sh"
CHROOT="/mnt"

# get directory of this script
dir=$(dirname $(readlink -f "$0"))

install_args(){
	if [ -z "$2" ];then 
		echo "Usage: $0 install <DRIVE>";
		exit 1
	fi

	SSD=$2
	echo "Installing Archlinux to the system to '${SSD}'"
}

# Check for UEFI mode
detect_uefi_system(){
	fw_platform_size="/sys/firmware/efi/fw_platform_size"
	if [ ! -s "${fw_platform_size}" ]; then
		echo "System is not booted in UEFI mode"
		exit 1
	fi
}

network_availablity_sync_time(){
	timedatectl set-ntp true
}

# Read variable
# Arg1: text to display
read_var(){
	var=""
	while [ -z "${var}" ]
	do
		read -p "Value for $1:" var
	done

	echo "${var}"
}

# Format drive and create new GPT and partitions ('EFI' and 'cryptsystem')
partition_drive(){
	# Create GPT header and partitions
	sgdisk --zap-all "${SSD}"
	sgdisk --clear \
		--new=1:0:+512MiB      --typecode=1:ef00 --change-name=1:EFI \
		--new=2:0:0            --typecode=2:8E00 --change-name=2:cryptsystem \
		"${SSD}"
	# Write changes to disc
	sync
	udevadm trigger
	# Format EFI partition as FAT32 filesystem
	mkfs.fat -F32 -n EFI /dev/disk/by-partlabel/EFI
}

yubikey_setup(){
	# Install tools for Yubikey to work
	pacman -Sy --noconfirm yubikey-manager yubikey-personalization pcsc-tools libu2f-host make json-c cryptsetup git make
	systemctl start pcscd.service

	# Clone Yubikey full disk encryption
	git clone https://github.com/agherzan/yubikey-full-disk-encryption.git /tmp/yubikey-full-disk-encryption
	cd /tmp/yubikey-full-disk-encryption
	make install

	while true
	do
		[ -n  "$(ykman list)" ] && break
		echo "Please insert Yubikey ..."
		sleep 5
	done
}

# Encrypts partition 'cryptsystem' with LUKS
encrypt_system_partition_with_yubikey(){

	ykfde-format --cipher aes-xts-plain64 --key-size 512 --hash sha512 /dev/disk/by-partlabel/cryptsystem
cat > /etc/ykfde.conf<<EOL
### Configuration for 'yubikey-full-disk-encryption'.

# Set to non-empty value to use 'Automatic mode with stored challenge (1FA)'.
YKFDE_CHALLENGE=""

# Use 'Manual mode with secret challenge (2FA)'.
YKFDE_CHALLENGE_PASSWORD_NEEDED="1"

# YubiKey slot configured for 'HMAC-SHA1 Challenge-Response' mode.
# Possible values are "1" or "2". Defaults to "2".
YKFDE_CHALLENGE_SLOT="2"

### OPTIONAL ###

# UUID of device to unlock with 'cryptsetup'.
# Leave empty to use 'cryptdevice' boot parameter.
YKFDE_DISK_UUID="$(blkid -s UUID -o value /dev/disk/by-partlabel/cryptsystem)"


# LUKS encrypted volume name after unlocking.
# Leave empty to use 'cryptdevice' boot parameter.
YKFDE_LUKS_NAME="system"
EOL
	ykfde-open -d /dev/disk/by-partlabel/cryptsystem -n system
}

# Create LVM on 'cryptsystem' partition and create logical partitions
init_system_partition(){
	root_size="40GB"
	swap_size="8GB"
	
	pvcreate --dataalignment 1m /dev/mapper/system
	vgcreate vg /dev/mapper/system
	lvcreate -L "${swap_size}" vg -n lv_swap
	lvcreate -L "${root_size}" vg -n lv_root
	lvcreate -l +100%FREE vg --name lv_home

	modprobe dm_mod
	# Scan for volumes and mount group
	vgscan
	vgchange -ay

	# Create swap partition
	mkswap -L swap /dev/vg/lv_swap
	# Mount swap (with TRIM support)
	swapon -d -L swap
	mkfs.ext4 /dev/vg/lv_root
	mkfs.ext4 /dev/vg/lv_home
}

# Mount system partitions
mount_partitions(){
	# Mount /
	mount /dev/vg/lv_root ${CHROOT}

	mkdir -p ${CHROOT}/boot/
	mkdir -p ${CHROOT}/home

	mount LABEL=EFI ${CHROOT}/boot/
	mount /dev/vg/lv_home ${CHROOT}/home
}

# Install base packages and chroot
bootstrap_install(){
	pkgs="base zsh net-tools wireless_tools wpa_supplicant dialog vim intel-ucode zsh"

	pacman-key --populate archlinux
	pacman-key --refresh-keys

	# Install base packages to /mnt
	pacstrap /mnt ${pkgs}

	# Generate filesystem table
	genfstab -U -p /mnt >> /mnt/etc/fstab
	echo "tmpfs	/tmp	tmpfs	defaults,noatime,mode=1777	0	0" >> /mnt/etc/fstab

	cp /etc/ykfde.conf /mnt/

	# Copy resurrection dir to /mnt
	rsync -a ${dir} ${CHROOT}

	arch-chroot "${CHROOT}" /resurrection/arch.sh chroot "${hname}" "${uname}"
}

postinstall(){
	systemd-nspawn -b -D /mnt /resurrection/arch.ch postinstall
}

install_args $@

hname=$(read_var "hostname")
uname=$(read_var "username")

network_availablity_sync_time
yubikey_setup
detect_uefi_system
partition_drive
encrypt_system_partition_with_yubikey
init_system_partition
mount_partitions
bootstrap_install

postinstall

#umount -R "${CHROOT}"
#swapoff -a
#reboot

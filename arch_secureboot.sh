#!/bin/bash

set -e

secure_boot_config(){
	ret=$(pacman -Qi efitools)
	[ "${ret}" -eq 1 ] && pacman -S efitools

	efikeys=/etc/efikeys
	[ -d ${efikeys} ] && mkdir ${efikeys}
	chmod -v 700 ${efikeys}
	cd ${efikeys}
	store_old_keys
}

store_old_keys(){
	efi-readvar -v PK -o old_PK.esl
	efi-readvar -v KEK -o old_KEK.esl
	efi-readvar -v db -o old_db.esl
	efi-readvar -v dbx -o old_dbx.esl
}

create_x509_certificates(){
	openssl req -new -x509 -newkey rsa:2048 -subj "/CN=cylon2p0's" platform key/ -keyout PK.key -out PK.crt -days 3650 -nodes -sha256
	openssl req -new -x509 -newkey rsa:2048 -subj "/CN=cylon2p0's" platform key/ -keyout KEK.key -out KEK.crt -days 3650 -nodes -sha256
	openssl req -new -x509 -newkey rsa:2048 -subj "/CN=cylon2p0's" kernel-signing key -keyout db.key -out db.crt -days 3650 -nodes -sha256
}

create_signed_signature_list(){
	cert-to-efi-sig-list -g $(uuidgen) PK.crt PK.esl
	sign-efi-sign-list -k PK.key -c PK.crt PK PK.esl PK.auth

	cert-to-efi-sig-list -g $(uuidgen) KEK.crt KEK.esl
	sign-efi-sign-list -a -k PK.key -c PK.crt KEK KEK.esl KEK.auth

	cert-to-efi-sig-list -g $(uuidgen) db.crt db.esl
	sign-efi-sign-list -a -k KEK.key -c KEK.crt db db.esl db.auth

	sign-efi-sign-list -k KEK.key -c KEK.crt dbx old_db.esl old_db.auth
}

create_der_format(){
	openssl x509 -outform DER -in PK.crt -out PK.crt
	openssl x509 -outform DER -in KEK.crt -out KEK.crt
	openssl x509 -outform DER -in db.crt -out db.crt
}

create_compund_esl(){
	cat old_KEK.esl KEK.esl > compound_KEK.esl
	cat old_db.esl db.esl > compound_db.esl 
	sign-efi-sig-list -k PK.key -c PK.crt KEK compound_KEK.esl compound_KEK.auth
	sign-efi-sig-list -k KEK.key -c KEK.crt db compound_db.esl compound_db.auth
}

remove_previous_keys(){
	# TODO: Merge with previous into branches
	cp -v KEK.esl compound_KEK.esl
	cp -v db.esl compound_db.esl
	sign-efi-sig-list -k PK.key -c PK.crt KEK compound_KEK.esl compound_KEK.auth
	sign-efi-sig-list -k KEK.key -c KEK.crt db compound_db.esl compound_db.auth
}

reboot(){
	efi-updatevar -e -f old_dbx.esl dbx
	efi-updatevar -e -f compound_db.esl db 
	efi-updatevar -e -f compound_KEK.esl KEK

	if [ -n "$FALLBACK_APPROACH" ]; then
		efi-updatevar -e -f old_dbx.esl dbx
		efi-updatevar -e -f old_db.esl db
		efi-updatevar -e -f old_KEK.esl KEK

		efi-updatevar -a -c db.crt db
		efi-updatevar -a -c KEK.crt KEK
	fi

	efi-updatevar -f PK.auth PK

	efi-readvar -v PK -o new_PK.esl
	efi-readvar -v KEK -o new_KEK.esl
	efi-readvar -v db -o new_db.esl
	efi-readvar -v dbx -o new_dbx.esl

	#// Or copy them to EFI and update from BIOS
	#// cp -v *.{auth,cer,crt,esl} /boot/efi/
}

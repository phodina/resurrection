![Toaster shopping](cylon.png)

# Resurrection

We all live in a matrix. Wouldn't it be nice not to start from scratch but setup the matrix the way we like it? . Once a while all of us need to (re)install our Linux system on a machine. There are many scripts that automate this, nevertheless most of them end with system booting and leaving you to do the rest yourself... unless you run `Resurrection`. This set of scripts offers you full LVM on LUKS hardened system with SecureBoot enabled as well as U2F for LUKS, login and running sudo commands. On top of that it also installs your dotfiles so you're ready to roll.

Currently UEFI system on x86_64 machine are supported. 

## Installation
### Download and verify

Download the `archlinux-<version>-x86_64.iso` and verify by running
```bash
gpg --keyserver pgp.mit.edu --keyserver-options auto-key-retrieve --verify archlinux-<version>-x86_64.iso.sig
```

* Add public keys into keyring
* Verify the files

### Put onto USB flash stick

Then prepare the USB flash stick:

```
dd bs=4M if=path/to/archlinux.iso of=/dev/sdx status=progress oflag=sync
```

## Contributing

Please read [CONTRIBUTING.md]() for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/phodina/resurrection/tags).

**NOTE: Will be used after the script installs minimal usable system.**

## Authors

- **Petr Hodina** - *Initial work* 

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://gist.github.com/PurpleBooth/LICENSE.md) file for details

## Resources

### Snapshots

### Sway
- https://geoff.greer.fm/2018/01/02/linux-laptop-locking/
- https://grimoire.science/working-with-wayland-and-sway/
- https://bennetthardwick.com/blog/2018-10-06-change-to-sway-arch-linux/
- https://github.com/addy-dclxvi/i3-starterpack

### Misc
- https://dougblack.io/words/a-good-vimrc.html

### Security
- https://wiki.archlinux.org/index.php/security
- https://theprivacyguide1.github.io/linux_hardening_guide.html

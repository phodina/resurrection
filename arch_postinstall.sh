#!/bin/sh

set -e

# Backup GPT to file
backup_gpt(){
	sgdisk -b="${SSD_GPT_BACKUP}" /dev/disk/by-partlabel/cryptsetup
}

yubikey_pam(){
	true
}

backup_2nd_slot(){
	true
	#ykpersonalize -v -2 -ochal-resp -ochal-hmac -ohmac-lt64 -ochal-btn-trig -oserial-api-visible
}
# Add backup key to LUKS slot in 'cryptsystem' partition
add_key2system_partition(){
	# Create a random key and store it in var
	# Encrypt the key with GPG
	# Store it to disk
	# Add it to LUKS slot

	## Create keyfile of 2048 random bytes
	#dd bs=512 count=4 if=/dev/urandom of="$SWAP_KEY" iflag=fullblock
	#cryptsetup luksAddKey /dev/disk/by-partlabel/cryptswap "$SWAP_KEY"
	true
}

install_pkgs(){
	pkgs="pass allacritty dmenu sway swaybg swaylock-effects-git swayidle wayland xorg-server wofi sddm mpv rsync slurp grim imv zathura ledger wireshark ntfs-3g yubico-pam pulseaudio"

	pacman -Sy --noconfirm ${pkgs}

	git clone https://aur.archlinux.org/yay.git /tmp/yay
	cd /tmp/yay
	makepkg -si --noconfirm

	pkgs="antibody starship"
	yay -Suy ${pkgs}
}

systemd(){
	systemctl enable NetworkManager
}

setup_dotfiles(){
	git clone --separate-git-dir=~/.origin git@gitlab.com/phodina/origin.git /tmp/origin
	rsync --recursive --verbose --exclude '.git' /tmp/origin ~/
	rm -r /tmp/origin

	vim +PlugInstall +qall > /dev/null
}

setup_secrets(){
	true
}

install_pkgs
systemd
setup_dotfiles

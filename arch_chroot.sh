#!/bin/bash

set -e

chroot_args(){
	if [ -z "$2" ] || [ -z "$3" ];then
		echo "Usage: $0 chroot <HNAME> <UNAME>";
		exit 1
	fi

	HNAME=$2
	UNAME=$3
}

set_locale(){
	# Select locale to generate
	echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen

	# Generate locale
	locale-gen
	echo LANG=en_US.UTF-8 > /etc/locale.conf
	export LANG=en_US.UTF-8
}

set_system_clock(){
	# Setup system clock
	ln -sf /usr/share/zoneinfo/Europe/Prague /etc/localtime
	hwclock --systohc --utc
}

set_hostname(){
	# Set the hostname
	echo "${HNAME}" > /etc/hostname
	## TODO: Populate also /etc/hosts
}

install_pkgs(){
	# Install rest of the system tools
	pkgs="sudo intel-ucode pkgfile efibootmgr intel-ucode base-devel btrfs-progs iw mkinitcpio lvm2 linux-hardened linux-hardened-headers linux-firmware make git yubikey-personalization cryptsetup udisks2 expect yubikey-full-disk-encryption dhcpcd wpa_supplicant networkmanager dhcpcd"

	pacman --noconfirm -S ${pkgs}

	mv /ykfde.conf /etc/

	# Update cache
	pkgfile --update
}

set_mkinitcpio(){
	# Replace hooks in /etc/mkinitcpio.conf
	sed -i 's/^HOOKS=.*/HOOKS=(base udev autodetect modconf block ykfde encrypt lvm2 filesystems keyboard fsck)/' /etc/mkinitcpio.conf

	# Regenerate initrd image
	mkinitcpio -p linux-hardened
}

set_bootloader(){
	## Install Systemd bootloader
	bootctl --path=/boot/ install

	# Get the UUID of your root partition
	UUID_SYSTEM=$(blkid -s UUID -o value /dev/disk/by-partlabel/cryptsystem)

	# Create boot entry
	mkdir -p /boot/loader/entries
	cat > /boot/loader/entries/arch.conf<<EOL
title Arch Linux
linux /vmlinuz-linux-hardened
initrd /intel-ucode.img
initrd /initramfs-linux-hardened.img
options root=/dev/mapper/vg-lv_root rw
EOL

	## TODO: Verify
	# Select default boot entry
	cat > /boot/loader/loader.conf<<EOL
default arch
timeout 3
editor no
EOL
}

setup_users(){
	## User and password management
	# Add user
	groupadd "${UNAME}"
	useradd -m -g "${UNAME}" -G wheel,wireshark,storage,power,users -s /bin/zsh "${UNAME}"

	echo "Set password for '${UNAME}' user:"
	passwd "${UNAME}"

	echo "Locking down root account"
	passwd -l root

	sed  -i 's/^# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/g' /etc/sudoers
}

parse_arg $@
set_locale
set_system_clock
set_hostname
install_pkgs
set_mkinitcpio
set_bootloader
setup_users

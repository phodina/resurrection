#!/bin/sh

# get directory of this script
dir=$(dirname $(readlink -f "$0"))

source_script(){
	script="${dir}/arch_$1.sh"
	if [ -e "${script}" ];then
		. ${script}
	else
		echo "Command [$1] not supported!"
		echo "usage: $0 (install|postinstall|nspawn)"
		exit 1
	fi
}

source_script $@
exit 0

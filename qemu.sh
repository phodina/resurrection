#!/bin/sh

if [ -z "$1" ];then
	echo "Usage: $0 <ARCH_ISO>"
    exit 1
fi

INSTALL_ISO=$1

QEMU_TARGET_DIR=/tmp/resurrection 
QEMU_DISK=${QEMU_TARGET_DIR}/arch_disk
QEMU_DISK_SIZE=10G
QEMU_MONITOR_PORT=10101

UEFI_VARS=${QEMU_TARGET_DIR}/qemu/efi_vars.bin
UEFI_VARS_DEFAULT=/usr/share/ovmf/x64/OVMF_VARS.fd
UEFI_CODE=/usr/share/ovmf/x64/OVMF_CODE.fd

# Verify image
gpg --keyserver pgp.mit.edu --keyserver-options auto-key-retrieve --verify "$INSTALL_ISO".sig
if [ "$?" -ne 0 ];then
	echo "Failed to verify installation ISO"
	exit 1
fi

# Create directory if necessary 
[ ! -d "${QEMU_TARGET_DIR}" ] && mkdir ${QEMU_TARGET_DIR}

# Check QEMU disk image exists 
if [ ! -f "${QEMU_DISK}" ];then
	qemu-img create -f qcow2 $QEMU_DISK $QEMU_DISK_SIZE
	# Disable CopyOnWrite
	chattr +C $QEMU_DISK
fi

# Check copy of UEFI variables exists
if [ ! -f "$UEFI_VARS" ];then
	# Check default UEFI variables provided by OVMF exist 
	if [ -f "$UEFI_VARS_DEFAULT" ];then
		cp $UEFI_VARS_DEFAULT $UEFI_VARS
	else
		echo "UEFI variables don't exist!"
		exit 1
	fi
fi

qemu-system-x86_64  -cdrom "$INSTALL_ISO"\
	-boot order=d -hda "$QEMU_DISK"\
	-m 4096M -enable-kvm -net nic,model=virtio\
	-net user,hostfwd=tcp::2222-:22\
	-monitor tcp:127.0.0.1:"$QEMU_MONITOR_PORT",server,nowait\
	-drive if=pflash,format=raw,readonly,file="$UEFI_CODE"\
	-drive if=pflash,format=raw,file="$UEFI_VARS"

#TODO: Create a loop and periodically write to device attached to QEMU behaving as serial port in guest and check for output in host
echo "Wait 50s unitl Archlinux boots to root tty ..."
sleep 50

#TODO: Ask user for password
#TODO: Generate keys files in tmp
# Connect to monitor - start SSH deamon
echo "sendkey s-y-s-t-e-m-c-t-l-spc-s-t-a-r-t-spc-s-s-h-d-kp_enter" | nc 127.0.0.1 "$QEMU_MONITOR_PORT" -q 3
echo "sendkey p-a-s" | nc 127.0.0.1 "$QEMU_MONITOR_PORT" -q 3
echo "sendkey s-w-d-kp_enter" | nc 127.0.0.1 "$QEMU_MONITOR_PORT" -q 3
echo "sendkey a-b-c-kp_enter" | nc 127.0.0.1 "$QEMU_MONITOR_PORT" -q 3
echo "sendkey a-b-c-kp_enter" | nc 127.0.0.1 "$QEMU_MONITOR_PORT" -q 3

# Clean known hosts
sed -i 's/^\[localhost\]:2222.*//' ~/.ssh/known_hosts

echo "Launching SSH session"
sshpass -p 'abc' ssh -o StrictHostKeyChecking=no root@localhost -p 2222
